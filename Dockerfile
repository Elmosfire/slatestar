FROM alpine:latest

RUN apk add sbcl
COPY ./ql .
RUN sbcl --script install.lisp

ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools
COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY ./src /src

RUN sbcl --script /src/packages.lisp



ENTRYPOINT [ "python", "src/app.py" ]

