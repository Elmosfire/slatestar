# Variables, keywords and proper literals

## Variables

Variables are any string of words that are not valid keywords.

If you put any of `a, an, the, my, your, our` before a keyword, you escape it and it becomes a variable.

So 

`rock` is not a valid variable but `a rock` is.
Not that the articles don't matter, `a rock` is the same variable as `the rock`

The keywords:

`it, he, she, him, her, they, them, ze, hir, zie, zir, xe, xem, ve, and ver` refer to the variable most recently set/changed.

## Types

Slatestar uses lisp types. Most common ones are booleans, integers, strings and lists.

## Constants

At the moment, there exist the following contants:

|name|value|
|---|---|
|mysterious| nil|
|silent| empty string|

# Specification of all lyrics

## *noun* assign *noun*
aliases:
 - `let *noun* be *noun*`
 - `may *noun* be *noun*`
 - `might *noun* be *noun*`

Sets a variable to a specific value (an expression)

## put *noun* into *noun*
aliases:
 - `*noun* into *noun*`

Sets a variable to a specific value (an expression)

## listen to *noun*
aliases:
 - `hear *noun*`

Stores a line from stdin into the variable (as a string type)

## shout *noun*
aliases:
 - `say *noun*`
 - `tell *noun*`
 - `yell *noun*`
 - `whisper *noun*`

Print a variable to stdout. Converts it into a string is necessairy.

## *noun* takes *noun* and *noun*
aliases:
 - `*noun* takes *noun* , *noun*`
 - `*noun* wants *noun* and *noun*`
 - `*noun* wants *noun* , *noun*`

Declares a function with a name, and named arguments

## *noun* taking *noun* and *noun*
aliases:
 - `*noun* taking *noun* , *noun*`

Calls a function with a given name and arguments

## send *noun*
aliases:
 - `give *noun*`
 - `return *noun*`
 - `send *noun* back`
 - `give *noun* back`
 - `return *noun* back`

return from the current function. 

## *noun* times *noun*
aliases:
 - `*noun* * *noun*`
 - `*noun* off *noun*`

Multiply two values

## *noun* between *noun*
aliases:
 - `*noun* / *noun*`
 - `*noun* over *noun*`

Divide two values

## *noun* plus *noun*
aliases:
 - `*noun* + *noun*`
 - `*noun* with *noun*`

Sums two values

## *noun* minus *noun*
aliases:
 - `*noun* - *noun*`
 - `*noun* without *noun*`

Substracts two values

## *noun* isnot *noun*
aliases:
 - `*noun* isnt *noun*`

Returns true if two expressions are not equal.

## *noun* is *noun*
aliases:
 - `*noun* are *noun*`
 - `*noun* was *noun*`
 - `*noun* were *noun*`

Returns true if two expressions are equal and the same type

## *noun* is greater than *noun*
aliases:
 - `*noun* is bigger than *noun*`
 - `*noun* is better than *noun*`
 - `*noun* is larger than *noun*`
 - `*noun* is higher than *noun*`
 - `*noun* is stronger than *noun*`
 - `*noun* are greater than *noun*`
 - `*noun* are bigger than *noun*`
 - `*noun* are better than *noun*`
 - `*noun* are larger than *noun*`
 - `*noun* are higher than *noun*`
 - `*noun* are stronger than *noun*`
 - `*noun* was greater than *noun*`
 - `*noun* was bigger than *noun*`
 - `*noun* was better than *noun*`
 - `*noun* was larger than *noun*`
 - `*noun* was higher than *noun*`
 - `*noun* was stronger than *noun*`
 - `*noun* were greater than *noun*`
 - `*noun* were bigger than *noun*`
 - `*noun* were better than *noun*`
 - `*noun* were larger than *noun*`
 - `*noun* were higher than *noun*`
 - `*noun* were stronger than *noun*`

Returns if first number is greater than the second number

## *noun* is smaller than *noun*
aliases:
 - `*noun* is smaller than *noun*`
 - `*noun* is lower than *noun*`
 - `*noun* is less than *noun*`
 - `*noun* is weaker than *noun*`
 - `*noun* are smaller than *noun*`
 - `*noun* are smaller than *noun*`
 - `*noun* are lower than *noun*`
 - `*noun* are less than *noun*`
 - `*noun* are weaker than *noun*`
 - `*noun* was smaller than *noun*`
 - `*noun* was smaller than *noun*`
 - `*noun* was lower than *noun*`
 - `*noun* was less than *noun*`
 - `*noun* was weaker than *noun*`
 - `*noun* were smaller than *noun*`
 - `*noun* were smaller than *noun*`
 - `*noun* were lower than *noun*`
 - `*noun* were less than *noun*`
 - `*noun* were weaker than *noun*`

Returns if first number is smaller than the second number

## *noun* or *noun*
aliases:

boolean or

## *noun* in_addition_to *noun*
aliases:
 - `*noun* in addition to *noun*`

boolean and

## not *noun*
aliases:

boolean not
## it
aliases:
 - `he`
 - `she`
 - `him`
 - `her`
 - `they`
 - `them`
 - `ze`
 - `hir`
 - `zie`
 - `zir`
 - `xe`
 - `xem`
 - `ve`
 - `ver`


no explanation found for this expression.

## roll *noun*
aliases:
 - `pull *noun*`
 - `pop *noun*`


no explanation found for this expression.

## split *noun*
aliases:


no explanation found for this expression.

## join *noun*
aliases:


no explanation found for this expression.

## *noun* at *noun*
aliases:


no explanation found for this expression.

## build *noun* up
aliases:


no explanation found for this expression.

## build *noun* up
aliases:


no explanation found for this expression.

## let *noun* be plus *noun*
aliases:
 - `let *noun* be + *noun*`
 - `let *noun* be with *noun*`
 - `may *noun* be plus *noun*`
 - `may *noun* be + *noun*`
 - `may *noun* be with *noun*`
 - `might *noun* be plus *noun*`
 - `might *noun* be + *noun*`
 - `might *noun* be with *noun*`


no explanation found for this expression.

## let *noun* be minus *noun*
aliases:
 - `let *noun* be - *noun*`
 - `let *noun* be without *noun*`
 - `may *noun* be minus *noun*`
 - `may *noun* be - *noun*`
 - `may *noun* be without *noun*`
 - `might *noun* be minus *noun*`
 - `might *noun* be - *noun*`
 - `might *noun* be without *noun*`


no explanation found for this expression.

## let *noun* be times *noun*
aliases:
 - `let *noun* be * *noun*`
 - `let *noun* be off *noun*`
 - `may *noun* be times *noun*`
 - `may *noun* be * *noun*`
 - `may *noun* be off *noun*`
 - `might *noun* be times *noun*`
 - `might *noun* be * *noun*`
 - `might *noun* be off *noun*`


no explanation found for this expression.

## let *noun* be between *noun*
aliases:
 - `let *noun* be / *noun*`
 - `let *noun* be over *noun*`
 - `may *noun* be between *noun*`
 - `may *noun* be / *noun*`
 - `may *noun* be over *noun*`
 - `might *noun* be between *noun*`
 - `might *noun* be / *noun*`
 - `might *noun* be over *noun*`


no explanation found for this expression.

## rock *noun*
aliases:
 - `push *noun*`


no explanation found for this expression.

## rock *noun* like *noun* and *noun*
aliases:
 - `rock *noun* like *noun* , *noun*`
 - `push *noun* like *noun* and *noun*`
 - `push *noun* like *noun* , *noun*`


no explanation found for this expression.

## if *noun*
aliases:


no explanation found for this expression.

## else
aliases:


no explanation found for this expression.

## while *noun*
aliases:


no explanation found for this expression.

