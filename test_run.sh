input_string=$(cat test/input.txt)
code_string=$(cat test/test.rock)
input_string_escaped=${input_string//$'\n'/¶}
code_string_escaped=${code_string//$'\n'/¶}

echo "{\"code\" : \"$code_string_escaped\", \"input\" : \"$input_string_escaped\"}" > tmp.json
curl -X GET -H "Content-type: application/json" "localhost:5000/exec" -d @tmp.json