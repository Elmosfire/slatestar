import re
import json
from pathlib import Path
import argparse
import subprocess
import os,binascii


root = Path(__file__).parent.parent

def open_src(fname, *args, **kwargs):
    return (root / "src"/ fname).open(*args, **kwargs)

def open_data(fname, *args, **kwargs):
    return (root / "data" / fname).open(*args, **kwargs)

def open_root(fname, *args, **kwargs):
    return (root / fname).open(*args, **kwargs)

with open_src("aliases.json") as file:
    ALIASES = json.load(file)
    for k in ALIASES:
        ALIASES[k].insert(0, k)
        
def keyword_escape(k, x):
    nm = x.group(2).replace(" ", "")
    return f"${k}|{nm}"        

def find_keyword(line):
    line = re.sub(r'\w+', lambda x: f"%{x.group(0)}|{x.group(0)}", line)
    print("?:", line)
    for k,v in ALIASES.items():
        for word in v:
            word = re.escape(word)
            prevline = line
            line = re.sub(rf"(?:\$|%)([a-z]+)\|({word})\b", lambda x: keyword_escape(k, x), line)
            if line != prevline:
                print("S:", line, "/", word)
    line = re.sub(rf"(%[a-zA-Z0-9_-]+)\|(\w+)", lambda x: x.group(1), line)
    print("?:", line)
    line = re.sub(r'".*?"', lambda x: f"{{{x.group(0)}}}", line)
    return line

def commas(line):
    line = re.sub(", and", " and", line)
    line = re.sub(", ", " , ", line)
    line = re.sub("\. ", " . ", line)
    return line


def numerix_literal(words):
    digits = "".join("." if x == "." else str((len(x)-1) %10) for x in words) + "."
    head, tail = digits.split(".", 1) 
    tail = tail.replace(".", "")
    if tail:
        tail = "." + tail
    
    return package(head + tail)

def literals(line):
    line_onlykw = [word.split("|")[0] for word in line]
    line_onlylit = [word.split("|")[-1] for word in line]
    if "$is" in line_onlykw:
        loc = line_onlykw.index("$is")
        if not any(x.startswith("$") for x in line_onlykw[:loc]):
            literals = numerix_literal(line_onlylit[loc+1:])
            return line[:loc] + ["$assign"] + [literals]
    if "$literal" in line_onlykw:
        loc = line_onlykw.index("$literal")
        literals = numerix_literal(line_onlylit[loc+1:])
        return line[:loc] + [literals]
    if "$says" in line_onlykw:
        loc = line_onlykw.index("$says")
        if not any(x.startswith("$") for x in line_onlykw[:loc]):
            literals = package('"' + " ".join(str(x).replace("%", '') for x in line_onlylit[loc+1:]) + '"')
            return line[:loc] + ["$assign"] + [literals]
    return line

    
def package(string):
    return "{" + string + "}"

def detect_variables(line):
    currently_reading = []
    
    for token in line:
        print(repr(token))
        if not re.match("^%[A-Za-z_]+$", token):
            if currently_reading:
                yield "{Ψ" + "_".join(currently_reading) + "}"
            currently_reading = []
            if re.match("^%[0-9.]+$", token):
                yield "{" + token[1:] + "}"
            else:
                yield token
        else:
            currently_reading.append(token[1:]) 
    if currently_reading:
        yield "{Ψ" + "_".join(currently_reading) + "}"

def preprocess(line):
    line = line.strip()
    print("0:", line)
    line = re.sub("\(.*?\)", " ", line.lower())
    print("1:", line)
    line = re.sub("'", "", line)
    print("2:", line)
    line = commas(line)
    print("3:", line)
    line = apply_escapes(line)
    print("4:", line)
    line = find_keyword(line)
    print("5:", line)
    line = re.sub("ESCAPE_ARTICLE_", '', line)
    print("6:", line)
    words = re.findall(r'(".*?"|\S+)', line)
    print("7:", words)
    words = literals(words)
    print("8:", words)
    words = list(detect_variables(words))
    print("9:", words)
    line = ' '.join(words)
    print("A:", line)
    line = re.sub("\|\w+", "", line)
    print("B:", line)
    line = re.sub("\$it", "{tmpvar}", line)
    print("C:", line)
    return line

def apply_escapes(line):
    for x in ["the", "a", "an", "your", "my", "our"]:
        line = re.sub(rf"\b{x}\b ", "ESCAPE_ARTICLE_", line)
    return line

REGEXES = []
with open_src("commands.txt") as file:
    for line in file:
        if not line.strip():
            continue
        lispfun, text = line.strip().split(":",1)
        reg = text.strip().replace("{}", r"\{([^{}]*?)\}")
        reg = re.sub("[A-Za-z_]+", lambda x:r"\$" + x.group(0).lower(), reg)
        if lispfun.startswith("%"):
            reg = "^" + reg + "$"
        for keyword in re.findall(r'([a-z_]+)', text):
            k = keyword.lower()
            if k not in ALIASES:
                ALIASES[k] = [k]
        REGEXES.append((lispfun,reg))
        

        
def apply_regexes(lines):
    close_stack = []
    last_function = ""
    
    for line in lines:
        line = preprocess(line)
        print("D:", line)
        if not line and close_stack:
            yield package(close_stack.pop())
            print("  close block")
        for k,v in REGEXES:
            def regmap(match):
                args = match.groups()
                args = [a for a in args if a is not None]
                print(">>", args)
                if k.startswith("%"):
                    
                    if k == "%defun":
                        return f"{{(defun {args[0]} (" + " ".join(args[1:]) + f") (progn}}|))|{args[0]}"
                    elif k == "%while":
                        return f"{{(loop while {args[0]} do (progn}}|))|{args[0]}"
                    elif k == "%return-from":
                        return f"{{({k[1:]} {last_function} " + " ".join(args) + ")}"
                    elif k == "%else":
                        return "{) (progn}"
                    else:
                        return f"{{({k[1:]} " + " ".join(args) + "(progn}|))"
                else:
                    return f"{{({k} " + " ".join(args) + ")}"
            prevline = line
            while True:
                line, *extra = re.sub(v, regmap, line).split("|")
                if line != prevline:
                    print(">:", line)
                    prevline = line
                else:
                    break
            if extra:
                close_stack.append(extra[0])
            if extra and extra[1:]:
                last_function = extra[1]
        yield line
    for x in close_stack:
        print("  close block")
        yield package(x)
        
        
def transpile(stream_in):
    with open_src("packages.lisp") as file:
        yield file.read()
    yield "\n\n;; engine starts here\n\n" 
    with open_src("engine.lisp") as file:
        yield file.read()
    yield "\n\n;; constants starts here\n\n" 
    with open_src("constants.lisp") as file:
        yield file.read() 
    yield "\n\n;; code starts here\n\n"
    
    LINES = []
    for line in stream_in:
        LINES.append(line)
    for line in apply_regexes(LINES):
        if not line.strip():
            continue
        if (line.startswith("{") and line.endswith("}")):
            yield line[1:-1] + "\n\n"
        else:
            assert False, f"could not transpile {line}"
            
def transpile_and_exec(stream_in, input_data):
    inputfile = f"input_{binascii.b2a_hex(os.urandom(15))}.txt"
    with open(inputfile, "w") as file:
        file.write(input_data)
    codefile = f"code_{binascii.b2a_hex(os.urandom(15))}.txt"
    with open(codefile, "w") as file:
        file.write('\n'.join(transpile(stream_in)).replace("(car *args*)", f"\"{inputfile}\""))
    
    proc = subprocess.Popen(("sbcl", "--script", codefile, inputfile), 
                      stdout=subprocess.PIPE,
                      stderr=subprocess.PIPE,
                      )
    stdout, stderr = proc.communicate()
    
    os.remove(inputfile)
    os.remove(codefile)
    
    print(stderr.decode())
    
    for x in stdout.decode().split("\n"):
        if x.startswith("OUT> "):
            yield x[5:]
        else:
            print(x)

    

       
def export_annotated_data():
    print(REGEXES)
    print(ALIASES)

    with open_data('regexes_transpiled.json', "w") as file:
        json.dump(REGEXES, file)
        
    with open_data('aliases_transpiled.json', "w") as file:
        json.dump(ALIASES, file)
        
    with open_data('regexes_cleaned.json', "w") as file:
        json.dump([(k,re.sub("[^A-Za-z{} _]", '', v)) for k,v in REGEXES], file)
        
       
if __name__ == "__main__": 
    
    parser = argparse.ArgumentParser(
                    prog = 'slatestar transpiler',
                    description = 'What the program does',
                    epilog = 'Text at the bottom of help')
    parser.add_argument('f_in')
    parser.add_argument('f_out')
    args = parser.parse_args()
    
    with open(args.f_out, "w") as f_out:  
        with open(args.f_in) as file:
            for line in transpile(file):
                f_out.write(line)
                
            
    
        

