from flask import Flask, request

from transpile import transpile_and_exec, transpile
app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello!'

@app.route('/exec', methods = ['GET', 'POST'])
def endpoint_execute():
    content_type = request.headers.get('Content-Type')
    if (content_type == 'application/json'):
        json = request.json
        input_data = json["input"].replace("¶", "\n")
        code_data = json["code"].replace("¶", "\n").split("\n")
        return transpile_and_exec(code_data, input_data)
    else:
        return 'Content-Type not supported!'
    
@app.route('/transpile', methods = ['GET', 'POST'])
def endpoint_transpile():
    content_type = request.headers.get('Content-Type')
    if (content_type == 'application/json'):
        json = request.json
        code_data = json["code"].replace("¶", "\n").split("\n")
        return transpile(code_data)
    else:
        return 'Content-Type not supported!'

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)