import json
from itertools import product
from pathlib import Path


from transpile import export_annotated_data

export_annotated_data()

root = Path(__file__).parent.parent

def open_src(fname, *args, **kwargs):
    return (root / "src"/ fname).open(*args, **kwargs)

def open_data(fname, *args, **kwargs):
    return (root / "data" / fname).open(*args, **kwargs)

def open_root(fname, *args, **kwargs):
    return (root / fname).open(*args, **kwargs)


with open_data("aliases_transpiled.json") as file:
    ALIASES = json.load(file)
        
ALIASES["{{}}"] = ["*noun*"]
    
with open_data("regexes_cleaned.json") as file:
    all_valid = {}
    for k,v in json.load(file):
        if k not in all_valid:
            all_valid[k] = []
        
        vwords = v.strip().split(" ")
        posibilities = [ALIASES.get(w, [w]) for w in vwords]
        print(posibilities)
        for x in product(*posibilities):
            all_valid[k].append(" ".join(x))
        
with open_src("short_explanation.md") as fin:
    with open_root("spec.md", "w") as fout:
        for line in fin:
            if line.startswith("## -"):
                key = line[4:].strip()
                first, *aliases = all_valid[key]
                del all_valid[key]
                fout.write(f"## {first}\n")
                fout.write(f"aliases:\n")
                for alias in aliases:
                    fout.write(f" - `{alias}`\n")
            else:
                fout.write(line)
        for key, (first, *aliases) in all_valid.items():
            fout.write(f"## {first}\n")
            fout.write(f"aliases:\n")
            for alias in aliases:
                fout.write(f" - `{alias}`\n")
                
            fout.write("\n\nno explanation found for this expression.\n\n")
