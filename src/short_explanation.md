# Variables, keywords and proper literals

## Variables

Variables are any string of words that are not valid keywords.

If you put any of `a, an, the, my, your, our` before a keyword, you escape it and it becomes a variable.

So 

`rock` is not a valid variable but `a rock` is.
Not that the articles don't matter, `a rock` is the same variable as `the rock`

The keywords:

`it, he, she, him, her, they, them, ze, hir, zie, zir, xe, xem, ve, and ver` refer to the variable most recently set/changed.

## Types

Slatestar uses lisp types. Most common ones are booleans, integers, strings and lists.

## Constants

At the moment, there exist the following contants:

|name|value|
|---|---|
|mysterious| nil|
|silent| empty string|

# Specification of all lyrics

## -setvar-star

Sets a variable to a specific value (an expression)

## -setvar-star-reverse

Sets a variable to a specific value (an expression)

## -listen-read

Stores a line from stdin into the variable (as a string type)

## -shout

Print a variable to stdout. Converts it into a string is necessairy.

## -%defun

Declares a function with a name, and named arguments

## -exec

Calls a function with a given name and arguments

## -%return-from

return from the current function. 

## -mult

Multiply two values

## -divide

Divide two values

## -plus

Sums two values

## -minus 

Substracts two values

## -isnot

Returns true if two expressions are not equal.

## -iseql

Returns true if two expressions are equal and the same type

## -greater

Returns if first number is greater than the second number

## -smaller

Returns if first number is smaller than the second number

## -or

boolean or

## -and

boolean and

## -not

boolean not
