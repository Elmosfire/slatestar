
(defparameter global-arg0 (format nil "~a" (car *args*)))
(defparameter tmpvar nil)

(defun load-input ()
    (with-open-file (stream global-arg0)
        (loop for line = (read-line stream nil)
            while line
            collect line
        )
    )
)

(defparameter input--- (load-input))

(defun read-from-input ()
    (progn
        (defparameter loc (car input---))
        (setq input--- (cdr input---))
        (format t "read: ~a" loc)
        (terpri)
        loc
    )
)

(defmacro setvar-star (varname value)
    `(progn (defparameter ,varname ,value) (defparameter tmpvar ,varname))
)

(defmacro process-star (value)
    `(progn (defparameter tmpvar ,value) tmpvar)
)

(defun tmp-var () tmpvar)

(defmacro listen-read (varname)
    `(defparameter ,varname (read-from-input))
)

(defmacro access-var (varname )
    `(progn (defvar ,varname nil) ,varname)
)

(defmacro exec (varname  &body body)
    `(,varname ,@body)
)

(defmacro modify-inplace (func a b)
    `(setvar-star ,a (apply ,func (list ,a ,b)))
)

(defmacro declare-modify-inplace (func)
    `(setvar-star ,a (apply ,func (list ,a ,b)))
)

(defun cast-to-number (a)
    (format t "casting ~S to number" a)
    (terpri)
    (if (numberp a)
        a
    (if (typep a 'string)
        (parse-integer a :junk-allowed t)
    (if (typep a 'list)
        (length a)
    )))
)

(defun cast-to-string (a)
    (format t "casting ~S to string" a)
    (format nil "~a" a)
)

(defun cast-to-number-or-string (a)
    (format t "casting ~S to number or string" a)
    (terpri)
    (if (numberp a)
        a
    (if (typep a 'string)
        a
    (if (typep a 'list)
        (length a)
    )))
)

(defun cast-validate (a b)
    (if (and (typep a 'string) (typep b 'string))
        (list a b)
        (list (cast-to-number a) (cast-to-number b))
    )
)

(defun apply-fixed (func-num func-string a b)
    (let (
        (args (cast-validate a b))
    )
    (if (numberp (car args))
        (progn 
            (format t "running function ~a (numbers)" func-num)
            (terpri)
            (apply func-num args)
        )
        (progn 
            (format t "running function ~a (string)" func-num)
            (terpri)
            (apply func-string args)
        )
    )
    )
)


(defun apply-fixed-numonly (func-num a b)
    (apply func-num (list (cast-to-number a) (cast-to-number b)))
)

(defun apply-fixed-stronly (func-num a b)
    (apply func-num (list (cast-to-number a) (cast-to-number b)))
)
    


(defun iseql-helper (a b)
    (if (null a)
    (if (null b)
        t
        nil
    )
    (if (null b)
        nil
        (= a b)
    )
    )
)

(defun iseql-helper-string (a b)
    (format t "~S ~S" a b)
    (if (null a)
    (if (null b)
        t
        nil
    )
    (if (null b)
        nil
        (string= a b)
    )
    )
)

(defun iseql (a b)
    (apply-fixed #'iseql-helper #'iseql-helper-string a b)
)


(defun concat-string (a b)
    (concatenate 'string a b)
)

(defun plus (a b)
    (apply-fixed #'+ #'concat-string a b)
)

(defmacro plus-inplace (a b)
    `(modify-inplace #'plus ,a ,b)
)

(defmacro increase (a)
    `(modify-inplace #'plus ,a 1)
)


(defun concat (a b)
    (apply-fixed-stronly #'concat-string a b)
)

(defun minus (a b)
    (apply-fixed-numonly #'- a b)
)

(defmacro minus-inplace (a b)
    `(modify-inplace #'minus ,a ,b)
)

(defmacro decrease (a)
    `(modify-inplace #'minus ,a 1)
)

(defun mult (a b)
    (apply-fixed-numonly #'* a b)
)

(defmacro mult-inplace (a b)
    `(modify-inplace #'mult ,a ,b)
)

(defun divide (a b)
    (apply-fixed-numonly #'/ a b)
)

(defmacro divide-inplace (a b)
    `(modify-inplace #'divide ,a ,b)
)

(defun greater (a b)
    (apply-fixed-numonly #'> a b)
)

(defun smaller (a b)
    (apply-fixed-numonly #'< a b)
)


(defun isnot (a b)
    (not (iseql a b))
)

(defun shout (a)
    (format t "OUT> ~a" a)
    (terpri)
)


(defmacro rock (varname expression)
    `(progn (defvar ,varname nil) (setvar-star ,varname (cons ,expression ,varname)))
)

(defmacro rock-only (varname)
    `(progn (setvar-star ,varname nil))
)

(defmacro roll (varname)
    `(progn (defparameter tmp (car ,varname)) (setvar-star ,varname (cdr ,varname)) (process-star tmp))
)

(defun split (line)
    "Splits a command string into tokens."
    (cl-ppcre:split " " line)
)

(defun join (seq)
    (process-star (apply #'concatenate (cons 'string (mapcar #'cast-to-string seq))))
)


(defun find-in-dict (key seq)
    (process-star (cdr (assoc key seq)))
)

(defun elt-custom (seq nth)
    (if (typep seq 'string)
        (subseq seq nth (+ nth 1))
        (elt seq nth)
    )
)